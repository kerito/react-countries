import { formatPopulation, ICountry, ICountryDto } from '@/entities/country'

export const mapCountry = (dto: ICountryDto): ICountry => {
  return {
    flagImage: {
      url: dto.flags.png,
      alt: dto.flags.alt,
    },
    name: dto.name.common,
    capital: dto.capital,
    population: formatPopulation(dto.population),
    region: dto.region,
  }
}
