import { useSearchParams } from 'react-router-dom'

type TQueryParams = {
  search?: string
  region?: string
}

const regions = [
  { value: 'africa', title: 'Africa' },
  { value: 'america', title: 'America' },
  { value: 'asia', title: 'Asia' },
  { value: 'europe', title: 'Europe' },
  { value: 'oceania', title: 'Oceania' },
]

export const SearchBar = () => {
  const [searchParams, setSearchParams] = useSearchParams()
  const search = searchParams.get('search') || ''
  const region = searchParams.get('region') || ''

  const queryParams: TQueryParams = {
    search,
    region,
  }

  const onChangeSearch = (value: string) => {
    const search = value || null
    const region = queryParams.region || null

    let params: undefined | Record<string, string>
    if (search || region) {
      params = { ...(search && { search }), ...(region && { region }) }
    }

    setSearchParams(params)
  }

  const onChangeRegion = (value: string) => {
    const region = value || null
    const search = queryParams.search || null

    let params: undefined | Record<string, string>
    if (region || search) {
      params = { ...(region && { region }), ...(search && { search }) }
    }
    setSearchParams(params)
  }

  return (
    <div>
      <input value={search} onChange={(e) => onChangeSearch(e.target.value)} />
      <select onChange={(e) => onChangeRegion(e.target.value)} defaultValue={queryParams.region}>
        <option value={''}>None</option>
        {regions.map((el) => (
          <option key={el.value} value={el.value}>
            {el.title}
          </option>
        ))}
      </select>
    </div>
  )
}
