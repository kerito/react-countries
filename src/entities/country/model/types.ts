export interface ICountry {
  flagImage: {
    url: string
    alt: string
  }
  name: string
  capital: string[]
  region: string
  population: string
}

export interface ICountryDetails {
  flagImage: {
    url: string
    alt: string
  }
  name: string
  nativeName: string[]
  region: string
  subRegion: string
  capital: string[]
  topLevelDomain: string[]
  currencies: string[]
  languages: string[]
  borders: string[]
  population: string
}
