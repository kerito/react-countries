import { CountryList } from '@/widgets'
import { useGetCountries } from '@/entities/country'
import { SearchBar } from '@/features/country'

const MainPage = () => {
  const { data: countries } = useGetCountries()
  return (
    <div>
      <SearchBar />
      <CountryList countries={countries} />
    </div>
  )
}

export default MainPage
