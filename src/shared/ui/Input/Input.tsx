import { FC, HTMLAttributes } from 'react'

interface IProps extends HTMLAttributes<HTMLInputElement> {}

export const Input: FC<IProps> = (props) => {
  return <input {...props} />
}
