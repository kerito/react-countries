import { ChangeTheme } from '@/features/theme'
import styles from './Header.module.scss'
import { Link } from 'react-router-dom'

export const Header = () => {
  return (
    <header className={`${styles.header} container`}>
      <Link to={'/'}>
        <h1>Where in the world?</h1>
      </Link>
      <ChangeTheme />
    </header>
  )
}
