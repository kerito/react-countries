import { useQuery } from '@tanstack/react-query'
import { getCountries, getCountryDetails, ICountry, ICountryDetails } from '@/entities/country'

export const useGetCountries = ({ region = 'all', enabled = true } = {}) => {
  return useQuery<ICountry[], void>({
    queryKey: ['countries'],
    queryFn: () => getCountries(region),
    enabled,
  })
}

export const useGetCountryDetails = (name: string) => {
  return useQuery<ICountryDetails, void>({
    queryKey: ['country', name],
    queryFn: () => getCountryDetails(name),
  })
}
