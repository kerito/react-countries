export interface ICountryDto {
  flags: {
    png: string
    alt: string
  }
  name: {
    common: string
  }
  capital: string[]
  region: string
  population: number
}

export interface ICountryDetailsDto {
  flags: {
    png: string
    alt: string
  }
  name: {
    common: string
    nativeName: {
      [key: string]: {
        common: string
      }
    }
  }
  region: string
  subregion: string
  capital: string[]
  tld: string[]
  currencies: {
    [key: string]: {
      name: string
    }
  }
  languages: { [key: string]: string }
  borders: string[]
  population: number
}
