import { FC, ReactNode, Suspense } from 'react'
import { Outlet } from 'react-router-dom'
import styles from './Layout.module.scss'

interface IProps {
  headerSlot?: ReactNode
}

export const Layout: FC<IProps> = ({ headerSlot }) => {
  return (
    <>
      {headerSlot && headerSlot}
      <main className={`${styles.main} container`}>
        <Suspense fallback={<div>loading...</div>}>
          <Outlet />
        </Suspense>
      </main>
    </>
  )
}
