import { FC, PropsWithChildren, useEffect } from 'react'
import { useThemeStore } from '@/entities/theme'

export const ThemeProvider: FC<PropsWithChildren> = ({ children }) => {
  const theme = useThemeStore((state) => state.theme)

  useEffect(() => {
    document.documentElement.setAttribute('data-theme', theme)
  }, [theme])

  return <>{children}</>
}
