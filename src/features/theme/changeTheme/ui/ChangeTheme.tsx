import { useThemeStore } from '@/entities/theme'
import { useShallow } from 'zustand/react/shallow'
import { MdDarkMode, MdOutlineDarkMode } from 'react-icons/md'
import styles from './ChangeTheme.module.scss'

export const ChangeTheme = () => {
  const { theme, toggleTheme } = useThemeStore(
    useShallow((state) => ({ theme: state.theme, toggleTheme: state.toggleTheme })),
  )

  return (
    <div onClick={toggleTheme} className={styles.container}>
      {theme == 'dark' ? (
        <>
          <MdDarkMode />
          <p>Dark Mode</p>
        </>
      ) : (
        <>
          <MdOutlineDarkMode />
          <p>Light Mode</p>
        </>
      )}
    </div>
  )
}
