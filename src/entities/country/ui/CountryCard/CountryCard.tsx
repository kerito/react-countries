import { FC } from 'react'
import { ICountry } from '@/entities/country'
import styles from './CountryCard.module.scss'
import { Link } from 'react-router-dom'

interface IProps {
  country: ICountry
}

export const CountryCard: FC<IProps> = ({ country }) => {
  return (
    <Link to={country.name}>
      <div className={styles.card}>
        <img src={country.flagImage.url} alt={country.flagImage.alt} />
        <div className={styles.body}>
          <h2>{country.name}</h2>
          <p>
            <span>Population: </span>
            {country.population}
          </p>
          <p>
            <span>Region: </span>
            {country.region}
          </p>
          <p>
            <span>Capital: </span>
            {country.capital}
          </p>
        </div>
      </div>
    </Link>
  )
}
