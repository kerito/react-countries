import ky from 'ky'

const APP_BASE_API = import.meta.env.VITE_APP_BASE_API

export const instance = ky.create({
  prefixUrl: APP_BASE_API,
})
