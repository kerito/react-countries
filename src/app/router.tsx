import React from 'react'
import { createBrowserRouter } from 'react-router-dom'
import { BaseLayout } from '@/app/layouts'

const MainPage = React.lazy(() => import('@/pages/MainPage/MainPage.tsx'))
const CountryPage = React.lazy(() => import('@/pages/CountryPage/CountryPage'))

export const router = createBrowserRouter([
  {
    element: <BaseLayout />,
    children: [
      {
        element: <MainPage />,
        path: '/',
      },
      {
        element: <CountryPage />,
        path: '/:name',
      },
    ],
  },
])
