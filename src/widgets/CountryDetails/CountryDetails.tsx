import { ICountryDetails } from '@/entities/country'
import { FC } from 'react'
import styles from './CountryDetails.module.scss'

interface IProps {
  countryDetails?: ICountryDetails
}
export const CountryDetails: FC<IProps> = ({ countryDetails }) => {
  if (!countryDetails) return <div></div>

  return (
    <div className={styles.container}>
      <img src={countryDetails.flagImage.url} alt={countryDetails.flagImage.alt} />
      <div className={styles.details}>
        <h1>{countryDetails.name}</h1>
        <div>
          <div>
            <p>
              <span>Native Name: </span>
              {countryDetails.nativeName}
            </p>
            <p>
              <span>Population: </span>
              {countryDetails.population}
            </p>
            <p>
              <span>Region: </span>
              {countryDetails.region}
            </p>
            <p>
              <span>Sub Region: </span>
              {countryDetails.subRegion}
            </p>
            <p>
              <span>Capital: </span>
              {countryDetails.capital.map((el) => el)}
            </p>
          </div>
          <div>
            <p>
              <span>Top Level Domain: </span>
              {countryDetails.topLevelDomain.map((el) => el)}
            </p>
            <p>
              <span>Currencies: </span>
              {countryDetails.currencies.map((el) => el)}
            </p>
            <p>
              <span>Languages: </span>
              {countryDetails.languages.map((el) => el)}
            </p>
          </div>
        </div>
        <div></div>
      </div>
    </div>
  )
}
