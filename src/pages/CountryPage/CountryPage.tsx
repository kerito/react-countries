import { useGetCountryDetails } from '@/entities/country'
import { useParams } from 'react-router-dom'
import { CountryDetails } from '@/widgets'

const CountryPage = () => {
  const { name } = useParams() as { name: string }
  const { data: countryDetails } = useGetCountryDetails(name)

  return <CountryDetails countryDetails={countryDetails} />
}

export default CountryPage
