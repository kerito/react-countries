export const formatPopulation = (number: number) => {
  return new Intl.NumberFormat().format(number)
}
