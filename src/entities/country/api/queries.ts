import {
  ICountry,
  ICountryDetails,
  ICountryDetailsDto,
  ICountryDto,
  mapCountry,
  mapCountryDetails,
} from '@/entities/country'
import { instance } from '@/shared/api/baseApi'

export const getCountries = async (region: string): Promise<ICountry[]> => {
  const currentUrl = region === 'all' ? region : `region/${region}`

  const response: ICountryDto[] = await instance
    .get(currentUrl, {
      searchParams: {
        fields: 'name,flags,capital,region,population',
      },
    })
    .json()

  return response.map(mapCountry)
}

export const getCountryDetails = async (name: string): Promise<ICountryDetails> => {
  const response: ICountryDetailsDto[] = await instance
    .get(`name/${name}`, {
      searchParams: {
        fullText: true,
        fields: 'flags,name,region,subregion,capital,tld,currencies,languages,borders,population',
      },
    })
    .json()

  return mapCountryDetails(response[0])
}
