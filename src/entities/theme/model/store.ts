import { create } from 'zustand'
import { TTheme } from '@/entities/theme'
import { createJSONStorage, persist } from 'zustand/middleware'

interface IThemeState {
  theme: TTheme
  toggleTheme: () => void
}

export const useThemeStore = create(
  persist<IThemeState>(
    (set) => ({
      theme: window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light',
      toggleTheme: () => set((state) => ({ theme: state.theme === 'dark' ? 'light' : 'dark' })),
    }),
    { name: 'theme', storage: createJSONStorage(() => localStorage) },
  ),
)
