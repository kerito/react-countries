import { CountryCard, ICountry } from '@/entities/country'
import styles from './CountryList.module.scss'
import { FC } from 'react'

interface IProps {
  countries?: ICountry[]
}

export const CountryList: FC<IProps> = ({ countries }) => {
  return (
    <div className={styles.list}>
      {countries &&
        countries.map((country) => <CountryCard key={country.name} country={country} />)}
    </div>
  )
}
