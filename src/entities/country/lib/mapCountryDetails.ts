import { formatPopulation, ICountryDetails, ICountryDetailsDto } from '@/entities/country'

export const mapCountryDetails = (dto: ICountryDetailsDto): ICountryDetails => {
  return {
    flagImage: {
      url: dto.flags.png,
      alt: dto.flags.alt,
    },
    topLevelDomain: dto.tld,
    capital: dto.capital,
    currencies: Object.values(dto.currencies).map((currency) => currency.name),
    name: dto.name.common,
    region: dto.region,
    subRegion: dto.subregion,
    nativeName: Object.values(dto.name.nativeName).map((key) => key.common),
    languages: Object.values(dto.languages).map((lang) => lang),
    borders: dto.borders,
    population: formatPopulation(dto.population),
  }
}
